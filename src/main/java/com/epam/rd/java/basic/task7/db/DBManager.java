package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static Connection connection = null;
	private static DBManager instance = null;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		String URL = null;
		try (InputStream inputStream = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(inputStream);
			URL = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
		connection = DriverManager.getConnection(URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		return findAll("user", "login", "users");
	}

	public boolean insertUser(User user) throws DBException {
		String getId = String.format("SELECT id FROM users WHERE login = '%s'", user.getLogin());
		String insertUser = String.format("INSERT INTO users (login) VALUES ('%s')", user.getLogin());
		return insert(insertUser, getId, user);
	}

	public boolean deleteUsers(User... users) throws DBException {
		String addForeignKey = "ALTER TABLE users_teams " +
				"ADD CONSTRAINT fk " +
				"FOREIGN KEY (user_id) " +
				"REFERENCES users(id)" +
				"ON DELETE CASCADE";
		String deleteForeignKey = String.format("ALTER TABLE users_teams drop foreign key %s", "fk");
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(addForeignKey);
			for (User user : users) {
				String deleteUserSQL = String.format("DELETE FROM users WHERE login = '%s'", user.getLogin());
				statement.executeUpdate(deleteUserSQL);
			}
			statement.executeUpdate(deleteForeignKey);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		String getUserSQL = String.format("SELECT id FROM users WHERE login = '%s'", login);
		User user = User.createUser(login);
		updateId(getUserSQL, user);
		return user;
	}

	public Team getTeam(String name) throws DBException {
		String getTeamSQL = String.format("SELECT id FROM teams WHERE name = '%s'", name);
		Team team = Team.createTeam(name);
		updateId(getTeamSQL, team);
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		return findAll("team", "name", "teams");
	}

	public boolean insertTeam(Team team) throws DBException {
		String getId = String.format("SELECT id FROM teams WHERE name = '%s'", team.getName());
		String insertTeam = String.format("INSERT INTO teams (name) VALUES ('%s')", team.getName());
		return insert(insertTeam, getId, team);
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			connection.setAutoCommit(false);

			for (Team team : teams) {
				String getTeamIdSQL = String.format("SELECT id FROM teams WHERE name = '%s'", team.getName());
				String getUserIdSQL = String.format("SELECT id FROM users WHERE login = '%s'", team.getName());

				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(getTeamIdSQL);
				if (rs.next())
					team.setId(rs.getInt("id"));

				rs = statement.executeQuery(getUserIdSQL);
				if (rs.next())
					user.setId(rs.getInt("id"));

				String insertSQL = String.format("INSERT INTO users_teams (user_id, team_id) VALUES (%s, %s)", user.getId(), team.getId());
				statement.executeUpdate(insertSQL);
				statement.close();
			}
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String getUserIdSQL = String.format("SELECT id FROM users WHERE login = '%s'", user.getLogin());
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(getUserIdSQL);
			if (rs.next()) {
				user.setId(rs.getInt("id"));
			}
			String getTeamsSQL = String.format("SELECT team_id FROM users_teams WHERE user_id = %s", user.getId());
			Statement statement1 = connection.createStatement();
			rs = statement1.executeQuery(getTeamsSQL);
			while (rs.next()) {
				Team team = Team.createTeam("temp");
				team.setId(rs.getInt("team_id"));
				String getTeamSQL = String.format("SELECT name FROM teams WHERE id = %s", team.getId());
				ResultSet name = statement.executeQuery(getTeamSQL);
				if (name.next()) {
					team.setName(name.getString("name"));
					teams.add(team);
				}
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String addForeignKey = "ALTER TABLE users_teams " +
				"ADD CONSTRAINT fk " +
				"FOREIGN KEY (team_id) " +
				"REFERENCES teams(id) " +
				"ON DELETE CASCADE ";
		String deleteForeignKey = String.format("ALTER TABLE users_teams drop foreign key %s", "fk");
		String deleteTeamSQL = String.format("DELETE FROM teams WHERE name = '%s'", team.getName());
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(addForeignKey);
			statement.executeUpdate(deleteTeamSQL);
			statement.executeUpdate(deleteForeignKey);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String updateNameSQL = String.format("UPDATE teams SET name = '%s' WHERE id = %s", team.getName(), team.getId());
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(updateNameSQL);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private <T> List<T> findAll(String type, String column, String table) {
		List<T> Ts = new ArrayList<>();
		String getId = String.format("SELECT %s FROM %s", column, table);
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(getId);
			while (rs.next()) {
				switch (type) {
					case "user":
						Ts.add((T)User.createUser(rs.getString(column)));
						break;
					case "team":
						Ts.add((T)Team.createTeam(rs.getString(column)));
						break;
				}
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Ts;
	}

	private boolean insert(String insertSQL, String getIdSQL, DBEntity entity) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(insertSQL);
			ResultSet rs = statement.executeQuery(getIdSQL);
			rs.next();
			entity.setId(rs.getInt(1));
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void updateId(String sql, DBEntity entity) {
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			rs.next();
			entity.setId(rs.getInt("id"));
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
